#include "icequbemanager.h"

#include <QNetworkInterface>

using namespace IceQube;

IceQubeManager::IceQubeManager(quint16 basePort)
   : m_findingCandidates(false),
     m_basePort(basePort)
{
}

void IceQubeManager::findCandidates()
{
   // Find local candidates
   foreach (QHostAddress address, QNetworkInterface::allAddresses())
   {
      m_hostCandidates.append(new Candidate(address, m_basePort + m_hostCandidates.count()));

      connect(m_hostCandidates.last(), SIGNAL(established()), SLOT(candidateEstablished()));
      emit candidateAvailable(m_hostCandidates.last());
   }

   // Start Stun and Turn clients
}

void IceQubeManager::addPeerCandidate(Candidate *candidate)
{
   m_peerCandidates.append(candidate);
}

void IceQubeManager::addStunServer(const QString &server)
{
   //Fixme: store server names or stun clients ?
   m_stunServers.append(server);
   if (m_findingCandidates)
   {
      //Start stun client for the given server
   }
}

void IceQubeManager::addTurnServer(const QString &server)
{
   //Fixme: store server names or turn clients ?
   m_turnServers.append(server);
   if (m_findingCandidates)
   {
      //Start stun client for the given server
   }
}

void IceQubeManager::candidateEstablished()
{
   m_hostEstablishedCandidates.append(static_cast<Candidate*>(sender()));
}
