#ifndef ICEQUBE_GLOBAL_H
#define ICEQUBE_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(ICEQUBE_LIBRARY)
#  define ICEQUBESHARED_EXPORT Q_DECL_EXPORT
#else
#  define ICEQUBESHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // ICEQUBE_GLOBAL_H
