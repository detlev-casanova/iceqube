#ifndef CANDIDATE_H
#define CANDIDATE_H

#include <QObject>
#include <QHostAddress>
#include <QTimer>

/**
 * This class manages actions taken by a candidate.
 * Depending on the direction, it will listen on the given address/port
 * or send data packets to try to contact the peer.
 *
 * TODO: a Candidate info class should be used to communicate with
 *       the program that uses the library. The idea is to avoid
 *       exposing all Candidate public methods to the program.
 */

namespace IceQube
{
class Candidate : public QObject
{
   Q_OBJECT
public:
   explicit Candidate(QObject *parent = 0);
   Candidate(const QHostAddress& address, quint16 port, QObject *parent = 0);

   void setAddress(const QHostAddress&);
   void setPort(quint16);

   QHostAddress address();
   quint16 port();

   void pingPeer();

signals:
   void established();
   void timedout();

public slots:

private slots:
   void dataIncoming();
   void sendData();

protected:
   QHostAddress m_hostAddress;
   quint16 m_port;
   QTimer pingTimer;

};
}

#endif // CANDIDATE_H
