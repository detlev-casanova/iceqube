#-------------------------------------------------
#
# Project created by QtCreator 2015-08-12T15:28:09
#
#-------------------------------------------------

QT       += network

QT       -= gui

TARGET = IceQube
TEMPLATE = lib

DEFINES += ICEQUBE_LIBRARY

SOURCES += \
    candidate.cpp \
    stunclient.cpp \
    turnclient.cpp \
    icequbemanager.cpp

HEADERS +=\
        iceqube_global.h \
    candidate.h \
    stunclient.h \
    turnclient.h \
    icequbemanager.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
