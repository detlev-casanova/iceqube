#include "candidate.h"

using namespace IceQube;

Candidate::Candidate(QObject *parent)
   : QObject(parent)
{

}

Candidate::Candidate(const QHostAddress &address, quint16 port, QObject *parent)
   : QObject(parent)
{
   m_hostAddress = address;
   m_port = port;
}

void Candidate::pingPeer()
{
   pingTimer.setInterval(100);
   connect(&pingTimer, SIGNAL(timeout()), SLOT(sendData()));
   pingTimer.start();
}

void Candidate::sendData()
{
   //Send data to m_hostAddress:m_port
}


void Candidate::dataIncoming()
{

}
