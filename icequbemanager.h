#ifndef ICEQUBEMANAGER_H
#define ICEQUBEMANAGER_H

#include <QObject>
#include <QStringList>

#include "iceqube_global.h"
#include "candidate.h"

namespace IceQube
{

class ICEQUBESHARED_EXPORT IceQubeManager : public QObject
{
   Q_OBJECT

public:
   IceQubeManager(quint16 basePort);
   void findCandidates();
   QList<Candidate*> getCandidates();
   void addPeerCandidate(Candidate* candidate);
   void addStunServer(const QString& server);
   void addTurnServer(const QString& server);

signals:
   void candidateAvailable(const Candidate*);

private slots:
   void candidateEstablished();

private:
   QStringList m_stunServers;
   QStringList m_turnServers;
   QList<Candidate*> m_hostCandidates;
   QList<Candidate*> m_peerCandidates;
   QList<Candidate*> m_hostEstablishedCandidates;

   bool m_findingCandidates;
   quint16 m_basePort;
};

}

#endif // ICEQUBEMANAGER_H
